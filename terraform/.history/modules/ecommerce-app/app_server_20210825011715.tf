####################################################################################################################################

#EC2 INSTANCES#

####################################################################################################################################

resource "aws_instance" "my_ec2_one" {
  ami                         = var.ami
  instance_type               = "t3a.small"
  subnet_id                   = aws_subnet.my_subnet_one_public.id
  vpc_security_group_ids      = [aws_security_group.security_group.id]
  key_name                    = "esgi-machines"
  associate_public_ip_address = true


  tags = {
    Name = "projet_ec2_instance_one"
  }
}

resource "aws_instance" "my_ec2_two" {
  ami                         = var.ami
  instance_type               = "t2.small"
  subnet_id                   = aws_subnet.my_subnet_two_public.id
  vpc_security_group_ids      = [aws_security_group.security_group.id]
  key_name                    = "esgi-machines"
  associate_public_ip_address = true


  tags = {
    Name = "projet_ec2_instance_two"
  }
}
