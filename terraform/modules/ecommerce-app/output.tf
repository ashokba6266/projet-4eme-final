output "instance_ip_addr_one" {
  value = aws_instance.my_ec2_one.private_ip
}

output "instance_ip_addr_two" {
  value = aws_instance.my_ec2_two.private_ip
}