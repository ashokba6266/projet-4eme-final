
variable "app_region" {
  type    = string
  default = "us-east-1"
}

variable "ami" {
  type    = string
  default = "ami-09e67e426f25ce0d7"
}

variable "instance_type" {
  type    = string
  default = "t2.micro"
}

variable "server_port" {
  type    = string
  default = "80"
}

variable "subnets_range"{
  type    = list(string)
  default = ["10.10.1.0/24","10.10.2.0/24"]  
}



