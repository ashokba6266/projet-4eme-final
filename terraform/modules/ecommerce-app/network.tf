######################################################################################################################################

# VPC # 

######################################################################################################################################


resource "aws_vpc" "projet_symfony_4" {
  cidr_block           = "10.10.0.0/16"
  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = {
    Name = "projet_symfony_vpc"
  }
}

######################################################################################################################################

# INTERNET GATEWAY # 

######################################################################################################################################

resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.projet_symfony_4.id

  tags = {
    Name = "main"
  }
}

######################################################################################################################################

# ROUTE TABLE # 

######################################################################################################################################


resource "aws_route_table" "route_table" {
  vpc_id = aws_vpc.projet_symfony_4.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }

  tags = {
    Name = "Route table for esgi project"
  }
}

resource "aws_route_table" "esgi_private_route_table" {
  vpc_id = aws_vpc.projet_symfony_4.id
}

######################################################################################################################################

# ROUTE TABLE ASSOCIATION # 

######################################################################################################################################

resource "aws_route_table_association" "public_one" {
  subnet_id      = aws_subnet.my_subnet_one_public.id
  route_table_id = aws_route_table.route_table.id
}

resource "aws_route_table_association" "public_two" {
  subnet_id      = aws_subnet.my_subnet_two_public.id
  route_table_id = aws_route_table.route_table.id
}


resource "aws_route_table_association" "private_one" {
  subnet_id      = aws_subnet.my_subnet_one_private.id
  route_table_id = aws_route_table.esgi_private_route_table.id
}

resource "aws_route_table_association" "private_two" {
  subnet_id      = aws_subnet.my_subnet_two_private.id
  route_table_id = aws_route_table.esgi_private_route_table.id
}


######################################################################################################################################

#APPLICATION LOAD BALANCER#

# Create a target group  

######################################################################################################################################


resource "aws_lb_target_group" "my-tg" {

  name        = "tf-lb"
  port        = var.server_port
  protocol    = "TCP"
  target_type = "instance"
  vpc_id      = aws_vpc.projet_symfony_4.id
}

# PLACE LOAD BALANCER IN FRONT OF EC2
resource "aws_lb_target_group_attachment" "group1" {
  target_group_arn = aws_lb_target_group.my-tg.id
  target_id        = aws_instance.my_ec2_one.id
  port             = "80"
}

# PLACE LOAD BALANCER IN FRONT OF EC2
resource "aws_lb_target_group_attachment" "group2" {
  target_group_arn = aws_lb_target_group.my-tg.id
  target_id        = aws_instance.my_ec2_two.id
  port             = "80"
}



# LOAD BALANCER
resource "aws_lb" "my-alb" {
  name               = "my-alb"
  internal           = false
  load_balancer_type = "network"
  subnets = [aws_subnet.my_subnet_one_public.id, aws_subnet.my_subnet_two_public.id]

  enable_deletion_protection = false
  ip_address_type            = "ipv4"   

  tags = {
    Environment = "production"
  }
}



# THIS DESCRIBES IN WHICH PORT THIS LOAD BALANCER IS GOING TO LISTEN 
resource "aws_lb_listener" "front_end" {
  load_balancer_arn = aws_lb.my-alb.id
  port              = "80"
  protocol          = "TCP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.my-tg.id 
  }
}

####################################################################################################################################

#SUBNETS#

####################################################################################################################################

resource "aws_subnet" "my_subnet_one_public" {
  vpc_id            = aws_vpc.projet_symfony_4.id
  cidr_block        = "10.10.1.0/24"
  availability_zone = "us-east-1a"

  tags = {
    Name = "esgi_subnet"
  }
}

resource "aws_subnet" "my_subnet_two_public" {
  vpc_id            = aws_vpc.projet_symfony_4.id
  cidr_block        = "10.10.2.0/24"
  availability_zone = "us-east-1b"

  tags = {
    Name = "esgi_subnet_two"
  }
}

resource "aws_subnet" "my_subnet_one_private" {
  vpc_id            = aws_vpc.projet_symfony_4.id
  cidr_block        = "10.10.3.0/24"
  availability_zone = "us-east-1a"

  tags = {
    Name = "esgi_subnet_two"
  }
}

resource "aws_subnet" "my_subnet_two_private" {
  vpc_id            = aws_vpc.projet_symfony_4.id
  cidr_block        = "10.10.4.0/24"
  availability_zone = "us-east-1b"

  tags = {
    Name = "esgi_subnet_two"
  }
}
