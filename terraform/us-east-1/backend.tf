terraform {
  backend "s3" {
    bucket  = "my-terarform-remote-state"
    key     = "tf/terraform.tf"
    region  = "us-east-1"
    encrypt = true
  }
}
