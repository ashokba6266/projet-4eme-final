<?php

namespace App\Entity;

use App\Repository\RatingRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=RatingRepository::class)
 */
class Rating
{
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Range(
     *      min = 0,
     *      max = 5,
     *      notInRangeMessage = "Your rating must be between {{ min }} and {{ max }}",
     * )
     * @Assert\NotBlank
     */
    private $rate;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank
     */
    private $comment;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="ratings")
     */
    private $author;

    /**
     * @ORM\Column(type="string")
     */
    private $targetEntity;

    /**
     * @ORM\Column(type="integer")
     */
    private $targetEntityId;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRate(): ?int
    {
        return $this->rate;
    }

    public function setRate(int $rate): self
    {
        $this->rate = $rate;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getAuthor(): ?User
    {
        return $this->author;
    }

    public function setAuthor(?User $author): self
    {
        $this->author = $author;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTargetEntity()
    {
        return $this->targetEntity;
    }

    /**
     * @param mixed $targetEntity
     */
    public function setTargetEntity($targetEntity): void
    {
        $this->targetEntity = $targetEntity;
    }

    /**
     * @return mixed
     */
    public function getTargetEntityId()
    {
        return $this->targetEntityId;
    }

    /**
     * @param mixed $targetEntityId
     */
    public function setTargetEntityId($targetEntityId): void
    {
        $this->targetEntityId = $targetEntityId;
    }

    public function __toString()
    {
        return $this->comment;
    }
}
