<?php

namespace App\Twig;

use App\Entity\User;
use App\Repository\ProductRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\PersistentCollection;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension
{

    /**@var \App\Repository\ProductRepository */
    private $productRepository;

    /**@var \App\Repository\UserRepository */
    private $userRepository;

    public function __construct(ProductRepository $productRepository, UserRepository $userRepository)
    {
        $this->productRepository = $productRepository;
        $this->userRepository = $userRepository;
    }

    public function getFilters(): array
    {
        return [
            new TwigFilter('filter_name', [$this, 'doSomething']),
        ];
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('isOwner', [$this, 'isOwner']),
            new TwigFunction('formatCollection', [$this, 'formatCollection']),
            new TwigFunction('findAllProducts', [$this, 'findAllProducts']),
        ];
    }

    /**
     * @param \App\Entity\User $user
     * @param $object
     * @return bool
     */
    public function isOwner(User $user, $object): bool
    {
        return $user->getId() === $object->getOwner()->getId();
    }

    /**
     * @param \Doctrine\ORM\PersistentCollection $collection
     * @return string
     */
    public function formatCollection(PersistentCollection $collection): string
    {
        if ($collection->count() === 0) {
            return '-';
        }

        return implode(', ', $collection->getValues());
    }

    /**
     * @return array
     */
    public function findAllProducts(): array
    {
        $products = $this->productRepository->findAll();
        $users = $this->userRepository->findAll();
        $data = array_merge($products, $users);
        sort($data);

        return $data;
    }
}
